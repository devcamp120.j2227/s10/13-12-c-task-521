import { TASK_CLICK, TASK_INPUT_CHANGE, TASK_ADD } from "../constants/task.constants"; 

const initialState = {
    input: "",
    tasks: [
    ]
}

const taskReducers = (state = initialState, action) => {
    switch (action.type) {
        case TASK_INPUT_CHANGE:
            state = {
                ...state,
                input: action.payload
            }
            break;
        case TASK_ADD:
            state = {
                ...state,
                input: "",
                tasks: [...state.tasks, {
                    id: state.tasks.length,
                    name: state.input,
                    done: false
                }]
            }
            break;
        case TASK_CLICK: 
            state.tasks[action.payload].done = !state.tasks[action.payload].done;
        
            break;
        default:
            break;
    }

    return {...state};
}

export default taskReducers;