import { combineReducers } from "redux";

import taskReducers from "./task.reducers";

// Tạo root reducer
const roorReducer = combineReducers({
    taskReducers
});

export default roorReducer;