import {
    TASK_CLICK,
    TASK_ADD,
    TASK_INPUT_CHANGE
} from "../constants/task.constants";

export const taskInputActionHandler = (value) => {
    return {
        type: TASK_INPUT_CHANGE,
        payload: value
    }
}   

export const taskAddClickHandler = () => {
    return {
        type: TASK_ADD
    }
}

export const taskToggleClickHandler = (id) => {
    return {
        type: TASK_CLICK,
        payload: id
    }
}